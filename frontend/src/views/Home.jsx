import React from 'react'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = theme => ({
  subtitle : {
    marginTop: theme.spacing.unit * 2
  }
});

const Home = (props) => {
  const { classes } = props;

  return (
    <Grid
    container
    spacing={0}
    direction="column"
    alignItems="center"
    justify="center"
    >
      <h1>Home</h1>
      <hr />
      <img src={require('../assets/images/bidon.png')} alt=""/>
      <Typography className={classes.subtitle} variant="h6" color="inherit" noWrap>
        Ecoshare is a platform for sharing goods and services for free. It's that simple.
        Sign up now and join the community !
      </Typography>
    </Grid> 
  )
}

export default withStyles(styles)(Home)