import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from './components/Header'
import SignIn from './views/SignIn';
import SignUp from './views/SignUp';
import Home from './views/Home';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#2D936C'
    },
    secondary: {
      main: '#5FAD41'
    },
    error: {
      main: '#545775'
    }
  },
});

function App() {
  return (
    <BrowserRouter>
      <div className='container'>
        <MuiThemeProvider theme={theme}>
          <Header />
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/signin' component={SignIn} />
            <Route exact path='/signup' component={SignUp} />
            <Route render={function () {
              return <p>Page not found...</p>
            }} />
          </Switch>
        </MuiThemeProvider>
      </div>
    </BrowserRouter>
  );
}

export default App;
