# TWEB Social Network

## Project description

### General idea

The main idea of this application is to provide a new social network as part of TWEB and MAC courses.

This will be a social network allowing people with the similar interests to get together and providing a platform where people can share goods, services and knowledge. For example, gardening amateurs can meet up through the application to organise workshops, share tools and experience with others.

Our goal is to provide a new kind of a platform, based on the core values like ecology, sharing, dialogue and friendship!

### Functionalities

#### Core

- User
  - Create a profile, with classic informations, avatar, etc.
  - Specify the domain of interests.

- Tag system to categorise the domains of interest

- Management of the groups of users, defined the by mutual interest

- Subscription/follow system
  - On tags, users and groups

- User should be able to write posts, with images, videos, links, etc.

#### Additional

- Map displaying the locations of the items/services the user is interested in, so the user can see what kind of people or goods there is in his area.

### Technologies

#### Frontend

- React

#### Backend

- NodeJS
- Express server
- Swagger to specify APIs
- REST API (graphQl if time allows it)

#### Persistence

- MongoDB or Neo4J

#### Testing

- Mocha and chai libraries

#### Workflow

- Gitlab with protected Master branch, issue and merge request system for each task

#### Hosting

- Heroku
