'use strict'

/* eslint-disable no-undef */
/* eslint-disable import/no-extraneous-dependencies */
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')

chai.use(chaiAsPromised)
chai.should()

// configure database connection
const neo4j = require('neo4j-driver').v1

const UserRepository = require('../repositories/user.js')

const graphenedbURL = 'bolt://localhost:7687'
const graphenedbUser = 'neo4j'
const graphenedbPass = '12345'
const driver = neo4j.driver(
  graphenedbURL,
  neo4j.auth.basic(graphenedbUser, graphenedbPass),
  { disableLosslessIntegers: true }
)

const repo = new UserRepository(driver)

describe('User repository', () => {

  describe('Create user', () => {

    it('should fail if email hash is missing', () => {
      repo.create({ password_hash: 'some value' }).should.be.rejected
    })

    it('should fail if password hash is missing', () => {
      repo.create({ email: 'some email' }).should.be.rejected
    })

    it('should return an object with an id property if all required fields are specified', (done) => {
      repo.create({ email: 'bob@marley.com', password_hash: '12345' })
        .then((user) => {
          user.should.have.property('id')
          done()
        })
    })
  })

  describe('Get users', () => {
    it('should return some users', (done) => {
      repo.getAll()
        .then((users) => {
          users.should.have.lengthOf.at.least(1)
          done()
        })
    })
  })

  describe('Check email', () => {
    it('should return false is email is not used', (done) => {
      repo.isEmailUsed('damian@marley.com')
        .then((used) => {
          used.should.be.false
          done()
        })
    })

    it('should return true is email is used', (done) => {
      repo.isEmailUsed('bob@marley.com')
        .then((used) => {
          used.should.be.true
          done()
        })
    })
  })

  describe('Authenticate user', () => {
    it('should fail if credentials are wrong', () => {
      repo.getByCredentials('bob@marley.com', 'abc').should.be.rejected
    })

    it('should return user if credentials are correct', (done) => {
      repo.getByCredentials('bob@marley.com', '12345')
        .then((user) => {
          user.should.have.property('id')
          done()
        })
    })

    it('should return user even if email case is not correct', (done) => {
      repo.getByCredentials('BOB@marley.com', '12345')
        .then((user) => {
          user.should.have.property('id')
          done()
        })
    })
  })

  describe('Update user', () => {
    it('should fail if id property is missing', () => {
      repo.update({ age: 42 }).should.be.rejected
    })

    it('should update existing attribute', (done) => {
      repo.getByCredentials('bob@marley.com', '12345')
        .then((user) => {
          user.password_hash = '6789'
          repo.update(user)
            .then((updated_user) => {
              updated_user.should.have.property('password_hash', '6789')
              done()
            })
        })
    })

    it('should add new attribute', (done) => {
      repo.getByCredentials('bob@marley.com', '6789')
        .then((user) => {
          user.age = 42
          repo.update(user)
            .then((updated_user) => {
              updated_user.should.have.property('age', 42)
              done()
            })
        })
    })
  })

  describe('Delete user', () => {
    it('should succeed if id does not exist', (done) => {
      repo.delete(-1)
        .then(() => done())
    })

    it('should succeed if id exist', (done) => {
      repo.getByCredentials('bob@marley.com', '6789')
        .then((user) => {
          repo.delete(user.id)
            .then(() => done())
        })
    })
  })
})

driver.close()
