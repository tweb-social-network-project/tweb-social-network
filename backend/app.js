const createError = require('http-errors')
const express = require('express')
const cors = require('cors')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const jwt = require('jsonwebtoken')

// configure database connection
const neo4j = require('neo4j-driver').v1

const config = require('./config')

const graphenedbURL = 'bolt://neo4j:7687'
const graphenedbUser = 'neo4j'
const graphenedbPass = '12345'
const driver = neo4j.driver(
  graphenedbURL,
  neo4j.auth.basic(graphenedbUser, graphenedbPass),
  { disableLosslessIntegers: true }
)

const UserRepository = require('./repositories/user.js')

const app = express()

const repo = new UserRepository(driver)

const corsOptions = {
  origin: '*'
}

app.use(cors(corsOptions))

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')
app.set('secret', config.secret)

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

/**
 * Unprotected routes
 */
app.get('/', (req, res, next) => { // eslint-disable-line no-unused-vars
  res.render('index', { title: 'Express' })
})

app.post('/signin', (req, res, next) => {
  repo.getByCredentials(req.body.email, req.body.password_hash)
    .then((result) => {
      const payload = { email: result.email }
      const token = jwt.sign(payload, app.get('secret'), {
        expiresIn: 60 * 60 * 24 // expires in 24 hours
      })
      // return the information including token as JSON
      res.json({
        success: true,
        token
      })
    })
    .catch(next)
})

app.post('/user', (req, res) => {
  repo
    .create({
      username: req.body.username,
      email: req.body.email,
      password_hash: req.body.password_hash
    })
    .then((user) => {
      res.send(user)
    })
})

app.use((req, res, next) => {
  const token = req.body.token || req.query.token || req.headers['x-access-token']

  if (token) {
    jwt.verify(token, app.get('secret'), (err, decoded) => {
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' })
      }
      req.decoded = decoded
      next()
      return null
    })
  } else {
    return res.status(403).send({
      success: false,
      message: 'No token provided.'
    })
  }
  return null
})

/**
 * Protected routes
 */
app.delete('/user', (req, res, next) => {
  repo.delete(req.body.id)
    .then(result => res.send(result))
    .catch(next)
})

app.get('/users', (req, res, next) => {
  repo.getAll()
    .then(result => res.send(result))
    .catch(next)
})

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404))
})

// error handler
app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

// Server
const server = app.listen(8081, () => {
  const host = server.address().address
  const port = server.address().port
  console.log('Node server listening at http://%s:%s', host, port) // eslint-disable-line no-console
})
