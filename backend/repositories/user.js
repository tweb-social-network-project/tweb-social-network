'use strict'

const { inspect } = require('util')

class UserRepository {
  constructor(driver) {
    this.driver = driver
  }

  create(user) {
    return new Promise((resolve, reject) => {
      // check required fields
      if ('email' in user === false) {
        reject(new Error('Email is missing'))
      }
      if ('password_hash' in user === false) {
        reject(new Error('Password hash is missing'))
      }

      const cypher = `CREATE (n:User ${inspect(user)}) RETURN n`
      const session = this.driver.session()
      session
        .run(cypher)
        .then((result) => {
          session.close()
          const createdUser = result.records[0]._fields[0].properties
          createdUser.id = result.records[0]._fields[0].identity
          resolve(createdUser)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  getByCredentials(email, passwordHash) {
    return new Promise((resolve, reject) => {
      const cypher = `MATCH (n:User) WHERE n.email =~ '(?i)${email}' AND n.password_hash = '${passwordHash}' RETURN n`
      const session = this.driver.session()
      session
        .run(cypher)
        .then((result) => {
          session.close()
          if (result.records.length > 0) {
            const user = result.records[0]._fields[0].properties
            user.id = result.records[0]._fields[0].identity
            resolve(user)
          } else {
            reject(new Error('No user found'))
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  update(user) {
    return new Promise((resolve, reject) => {
      const cypher = `MATCH (n:User) WHERE ID(n) = ${user.id} SET n = ${inspect(user)} RETURN n`
      const session = this.driver.session()
      session
        .run(cypher)
        .then((result) => {
          session.close()
          const updatedUser = result.records[0]._fields[0].properties
          updatedUser.id = result.records[0]._fields[0].identity
          resolve(updatedUser)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  delete(id) {
    return new Promise((resolve, reject) => {
      const cypher = `MATCH (n:User) WHERE ID(n) = ${id} DELETE n`
      const session = this.driver.session()
      session
        .run(cypher)
        .then((result) => {
          session.close()
          resolve(result)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  getAll() {
    return new Promise((resolve, reject) => {
      const cypher = 'MATCH (n:User) RETURN n'
      const session = this.driver.session()
      session
        .run(cypher)
        .then((result) => {
          session.close()
          const users = result.records.map(
            user => Object.assign(
              { id: user._fields[0].identity },
              user._fields[0].properties
            )
          )
          resolve(users)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  isEmailUsed(email) {
    return new Promise((resolve, reject) => {
      const cypher = `MATCH (n:User) WHERE n.email =~ '(?i)${email}' RETURN n`
      const session = this.driver.session()
      session
        .run(cypher)
        .then((result) => {
          session.close()
          resolve(result.records.length > 0)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

module.exports = UserRepository